import datetime
import sqlite3

#acquisisci data, orario
data_completa = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")

#mettili in due stringhe
orario = data_completa[11:] #la data occupa i primi 10 caratteri (0..9) + 1 spazio bianco
giorno = data_completa[:10]

#stampali
print(giorno+" "+orario)

#collegati al database
path ="database.db"
connect = sqlite3.connect(path)

c = connect.cursor()    #cursore per eseguire comandi SQL

c.execute("INSERT INTO RESOCONTO(GIORNO, ORARIO) VALUES(?,?)", (giorno,orario)) #inseriti come tuplets 
                                                                               #(mettere la virgola anche se c'è un solo valore)                              
                                                                    
connect.commit()  #salva
connect.close()   #chiudi

#tutto okay fino a qui 22/07/2018

print("Dati salvati")
