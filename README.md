What is this project about?

I wanted to create a program to register at what time of the day I go to sleep everyday in order to do a graph and do some calculations on it like find out what's the average time i go to sleep.
Then we could push it even further and creating some more graphs to study more things about sleeping.
I used a very simple database made with sqlite that holds the data; it is composed by three records:
1) A primary key
2) The date of the submission
3) The hour of the submission (so at what hour i went to sleep)

Then there are two py files:
1) raccolta_new.py which collect the information(current data and time) and put them in the database
2) analisi.py which has the task of creating the graph 

The graph must have progressive dates on the X and the hours on the Y so as we can see the tendency of the user sleep hours
